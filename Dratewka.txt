Fiszka do projektu "Dratewka".

Imię: Tadeusz
Nazwisko: Kantor
Klasa: 3IB1
Termin (np. 50%): 100%

poniżej zaznacz jednoznacznie co zrobiłeś/aś w 100%. Plik umieść w folderze z grą.
-------------

1. wprowadzanie komunikatów (0.5p) TAK
	- komunikaty wpisywane z klawiatury (zatwierdzanie Enterem) TAK
	- rozpoznanie co najmniej trzech komunikatów (co najmniej informacja w konsoli) TAK
	- wyglad prompta, jego zachwanie (brak możliwości "wyjścia") TAK
	- po wejściu do gry domyślnie wpisuje duże znaki (caps/shift - zmiana) TAK

2. mapa, ruch (1p) TAK
	- możliwość przechodzenia po dostępnych lokalizacjach (E/W/N/S), pokazywanie odpowiedniej grafiki i nazw lokalizacji TAK
	- pokazanie odpowiednich kierunków na kompasie + "You can go" TAK

3. przemieszczanie przedmiotów (max 1.5p) TAK
 (0.5p)	- rozmieszczone przedmioty początkowe - "You see" TAK
 (1p)	- działanie polecenia T(TAKE) / D(DROP) - "You see" + "You are carrying" (z obostrzeniami) TAK

4. komunikaty (1p) TAK
	- pokazywanie odpowiednich komunikatów co najmniej trzech poleceń (np. przy przechodzeniu po lokalizacjach, use, vocabulary itp.) TAK
	- co najmniej dwa wybrane komunikaty "z timeoutem" (np. próba wejścia na lokację za smokiem) TAK

5. logika gry (max 2.5p) TAK
 (1p)	- polecenie U(USE) (odpowiedni komunikat, uzyskanie przedmiotów - doprowadzenie do conajmniej jednej części owcy) TAK
 (1.5p)	- oprogramowanie wszystkich zależności (przedmioty) doprowadzających do ukończenia gry, możliwość ukończenia gry (bez "oszustw") - cała logika działa TAK
		
6. dodatki (0.5p) TAK
	- czołówka, instrukcja gry TAK (Czołówkę można pominąć klawiszem Escape)
	- hejnał TAK (Dźwięk nie działa w Chrome, ale w innych przeglądarkach tak)
	[Działa w Chrome po ustawieniu flagi o autoodtwarzaniu(chrome://flags/#autoplay-policy) na "No user gesture is required"]
	- polecenia V(VOCABULARY), G(GOSSIPS) TAK
	

SUMA PUNKTÓW: 7p