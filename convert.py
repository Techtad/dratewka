import io

map = []
for i in range(7):
    map.append([])
    for j in range(6):
        map[i].append("")
action = 'SCAN'
x = 0
y = 0

with open('dane_lokacji_1.txt', 'r', encoding='utf-8') as file:
    for line in file:
        if action == 'SCAN':
            if line[:6] == 'WIERSZ':
                y = int(line[7]) - 1
                x = 0
                if y == 4 or y == 5:
                    x = 3
                action = 'ROW'
                continue
        elif action == 'ROW':
            if len(line) < 2:
                action = 'SCAN'
                continue
            map[x][y] = line[:-1]
            x += 1
        print(line[:-1], action)

del x
del y
del action
print(map)

crossings = [
    [
        '{ North: false, East: true, South: false, West: false }',
        '{ North: false, East: true, South: true, West: false }',
        '{ North: true, East: true, South: false, West: false }',
        '{ North: false, East: true, South: false, West: false }'
    ],
    [
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: true, South: true, West: true }',
        '{ North: true, East: false, South: false, West: true }',
        '{ North: false, East: true, South: false, West: true }'
    ],
    [
        '{ North: false, East: true, South: true, West: true }',
        '{ North: true, East: true, South: true, West: true }',
        '{ North: true, East: false, South: true, West: false }',
        '{ North: true, East: false, South: false, West: true }'
    ],
    [
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: true, South: false, West: false }',
        '{ North: false, East: true, South: false, West: false }',
        '{ North: false, East: true, South: false, West: false }',
        '{ North: false, East: true, South: false, West: false }'
    ],
    [
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: true, South: true, West: true }',
        '{ North: true, East: false, South: true, West: true }',
        '{ North: true, East: true, South: true, West: true }',
        '{ North: true, East: false, South: true, West: true }',
        '{ North: true, East: true, South: false, West: true }'
    ],
    [
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: true, South: false, West: true }',
        '{ North: false, East: false, South: true, West: false }',
        '{ North: true, East: true, South: false, West: true }',
        '{ North: false, East: false, South: true, West: false }',
        '{ North: true, East: true, South: false, West: true }'
    ],
    [
        '{ North: false, East: false, South: true, West: true }',
        '{ North: true, East: false, South: false, West: true }',
        '{ North: false, East: false, South: true, West: false }',
        '{ North: true, East: false, South: true, West: true }',
        '{ North: true, East: false, South: false, West: false }',
        '{ North: false, East: false, South: false, West: true }'
    ],
]
print(len(map[1]))

with open('map.js', 'w') as file:
    file.write("map: [\n")
    for x, row in enumerate(map):
        file.write('    [\n')
        for y, column in enumerate(row):
            parameters = column.split(", ")
            print(x, y, len(parameters), parameters)
            if len(parameters) == 3:
                file.write('        new Location(\"%s\", \"%s\", \"%s\", %s),\n' % (
                    parameters[0], parameters[1], parameters[2], crossings[x][y]))
        file.write('    ],\n')
    file.write(']')
