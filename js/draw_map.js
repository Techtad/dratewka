document.addEventListener("DOMContentLoaded", function (event) {
    let container = document.getElementById("map")
    for (y = 1; y <= 6; y++) {
        for (x = 1; x <= 7; x++) {
            let imgSrc = DratewkaMap.At(x, y) == undefined ? "" : DratewkaMap.At(x, y).getImage()
            let bgColor = DratewkaMap.At(x, y) == undefined ? "white" : DratewkaMap.At(x, y).color
            let image = document.createElement("img")
            image.setAttribute("src", imgSrc)
            image.style.backgroundColor = bgColor
            container.append(image)
        }
    }

    for (y = 1; y <= 6; y++) {
        for (x = 1; x <= 7; x++) {
            if (DratewkaMap.At(x, y) == undefined) continue

            if (DratewkaMap.At(x, y).hasPassage("East")) {
                let passage = document.createElement("div")
                passage.innerText = "E"
                passage.classList.add("passage")
                passage.style.left = (x * 168 - 20) + "px";
                passage.style.top = ((y - 1) * 120 + 50) + "px";
                passage.style.backgroundColor = "green"
                container.append(passage)
            }
            if (DratewkaMap.At(x, y).hasPassage("West")) {
                let passage = document.createElement("div")
                passage.innerText = "W"
                passage.classList.add("passage")
                passage.style.left = ((x - 1) * 168) + "px";
                passage.style.top = ((y - 1) * 120 + 50) + "px";
                passage.style.backgroundColor = "blue"
                container.append(passage)
            }
            if (DratewkaMap.At(x, y).hasPassage("North")) {
                let passage = document.createElement("div")
                passage.innerText = "N"
                passage.classList.add("passage")
                passage.style.left = ((x - 1) * 168 + 74) + "px";
                passage.style.top = ((y - 1) * 120) + "px";
                passage.style.backgroundColor = "red"
                container.append(passage)
            }
            if (DratewkaMap.At(x, y).hasPassage("South")) {
                let passage = document.createElement("div")
                passage.innerText = "S"
                passage.classList.add("passage")
                passage.style.left = ((x - 1) * 168 + 74) + "px";
                passage.style.top = (y * 120 - 20) + "px";
                passage.style.backgroundColor = "yellow"
                container.append(passage)
            }
        }
    }
})