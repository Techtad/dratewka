function DratewkaLocation(text, img, color, passages) {
    this.text = text
    this.img = img
    this.color = color
    this.passages = passages

    this.getImage = function () {
        return "img/" + this.img
    }

    this.hasPassage = function (direction) {
        return this.passages[direction]
    }
}

var DratewkaMap = {
    map: [
        [
            new DratewkaLocation("You are inside a brimstone mine", "11.gif", "rgb(235,211,64)", { North: false, East: true, South: false, West: false }),
            new DratewkaLocation("A man nearby making tar", "21.gif", "rgb(255,190,99)", { North: false, East: true, South: true, West: false }),
            new DratewkaLocation("You are by the Vistula River", "31.gif", "rgb(122,232,252)", { North: true, East: true, South: false, West: false }),
            new DratewkaLocation("You are in the Wawel Castle", "41.gif", "rgb(255,176,141)", { North: false, East: true, South: false, West: false }),
        ],
        [
            new DratewkaLocation("You are at the entrance to the mine", "12.gif", "rgb(89,93,87)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("A timber yard", "22.gif", "rgb(255,190,99)", { North: false, East: true, South: true, West: true }),
            new DratewkaLocation("You are by the Vistula River", "32.gif", "rgb(140,214,255)", { North: true, East: false, South: false, West: true }),
            new DratewkaLocation("You are inside a dragon's cave", "42.gif", "rgb(198,205,193)", { North: false, East: true, South: false, West: true }),
        ],
        [
            new DratewkaLocation("A hill", "13.gif", "rgb(117,237,243)", { North: false, East: true, South: true, West: true }),
            new DratewkaLocation("You are by a roadside shrine", "23.gif", "rgb(167,245,63)", { North: true, East: true, South: true, West: true }),
            new DratewkaLocation("You are on a bridge over river", "33.gif", "rgb(108,181,242)", { North: true, East: false, South: true, West: false }),
            new DratewkaLocation("A perfect place to set a trap", "43.gif", "rgb(255,176,141)", { North: true, East: false, South: false, West: true }),
        ],
        [
            new DratewkaLocation("Some bushes", "14.gif", "rgb(202,230,51)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("You are by a small chapel", "24.gif", "rgb(212,229,36)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("You are by the old tavern", "34.gif", "rgb(255,189,117)", { North: false, East: true, South: false, West: false }),
            new DratewkaLocation("You are by the water mill", "44.gif", "rgb(255,190,99)", { North: false, East: true, South: false, West: false }),
            new DratewkaLocation("You are by a swift stream", "54.gif", "rgb(108,181,242)", { North: false, East: true, South: false, West: false }),
            new DratewkaLocation("You are in a bleak funeral house", "64.gif", "rgb(254,194,97)", { North: false, East: true, South: false, West: false }),
        ],
        [
            new DratewkaLocation("An old deserted hut", "15.gif", "rgb(220,204,61)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("You are on a road leading to a wood", "25.gif", "rgb(167,245,63)", { North: false, East: true, South: true, West: true }),
            new DratewkaLocation("You are at the town's end", "35.gif", "rgb(255,190,99)", { North: true, East: false, South: true, West: true }),
            new DratewkaLocation("You are at a main crossroad", "45.gif", "rgb(255,190,99)", { North: true, East: true, South: true, West: true }),
            new DratewkaLocation("You are on a street leading forest", "55.gif", "rgb(255,190,99)", { North: true, East: false, South: true, West: true }),
            new DratewkaLocation("You are on a path leading to the wood", "26 i 65.gif", "rgb(167,245,63)", { North: true, East: true, South: false, West: true }),
        ],
        [
            new DratewkaLocation("The edge of a forest", "16.gif", "rgb(167,245,63)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("You are in a forest", "26 i 65.gif", "rgb(167,245,63)", { North: false, East: true, South: false, West: true }),
            new DratewkaLocation("You are in a butcher's shop", "36.gif", "rgb(255,188,102)", { North: false, East: false, South: true, West: false }),
            new DratewkaLocation("You are on a town street", "46.gif", "rgb(255,190,99)", { North: true, East: true, South: false, West: true }),
            new DratewkaLocation("You are in a woodcutter's backyard", "56.gif", "rgb(255,190,99)", { North: false, East: false, South: true, West: false }),
            new DratewkaLocation("You are at the edge of a forest", "66.gif", "rgb(167,245,63)", { North: true, East: true, South: false, West: true }),
        ],
        [
            new DratewkaLocation("A dark forest", "17.gif", "rgb(140,253,99)", { North: false, East: false, South: true, West: true }),
            new DratewkaLocation("You are in a deep forest", "27 i 67.gif", "rgb(140,253,99)", { North: true, East: false, South: false, West: true }),
            new DratewkaLocation("You are in a cooper's house", "37.gif", "rgb(255,188,102)", { North: false, East: false, South: true, West: false }),
            new DratewkaLocation("You are in a frontyard of your house", "47.gif", "rgb(255,190,99)", { North: true, East: false, South: true, West: true }),
            new DratewkaLocation("You are in a shoemaker's house", "57.gif", "rgb(254,194,97)", { North: true, East: false, South: false, West: false }),
            new DratewkaLocation("You are in a deep forest", "27 i 67.gif", "rgb(140,253,99", { North: false, East: false, South: false, West: true }),
        ],
    ],

    At: function (x, y) {
        return this.map[x - 1][y - 1]
    }
}