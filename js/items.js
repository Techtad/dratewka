function Item(id, label, interactive, name) {
    this.id = id
    this.label = label
    this.interactive = interactive
    this.name = name
}

var DratewkaItems = {
    db: [],
    map: [],

    getById: function (id) {
        return this.db[id]
    },

    getByName: function (name) {
        return this.db.find((item) => { return item != undefined && item.name.toUpperCase() == name.toUpperCase() })
    },

    atLocation: function (x, y) {
        return this.map[x][y]
    },

    isAtLocation: function (x, y, name) {
        let itemFound = false
        for (let item of this.atLocation(x, y)) {
            if (item.name.toUpperCase() == name.toUpperCase())
                itemFound = true
        }
        return itemFound
    },

    addToLocationByName: function (x, y, name) {
        this.map[x][y].push(this.getByName(name))
    },

    addToLocationById: function (x, y, id) {
        this.map[x][y].push(this.getById(id))
    },

    tryRemoveFromLocation: function (x, y, name) {
        if (this.isAtLocation(x, y, name)) {
            if (!DratewkaItems.getByName(name).interactive) return false

            let itemIndex = this.atLocation(x, y).indexOf(DratewkaItems.getByName(name))
            this.atLocation(x, y).splice(itemIndex, 1)
            return true
        } else
            return false
    },

    removeSheepPart: function (x, y, name) {
        if (this.isAtLocation(x, y, name)) {
            let itemIndex = this.atLocation(x, y).indexOf(DratewkaItems.getByName(name))
            this.atLocation(x, y).splice(itemIndex, 1)
            return true
        } else
            return false
    }
}
DratewkaItems.db[10] = new Item(10, "a KEY", true, "KEY")
DratewkaItems.db[11] = new Item(11, "an AXE", true, "AXE")
DratewkaItems.db[12] = new Item(12, "STICKS", true, "STICKS")
DratewkaItems.db[13] = new Item(13, "sheeplegs", false, "sheeplegs")
DratewkaItems.db[14] = new Item(14, "MUSHROOMS", true, "MUSHROOMS")
DratewkaItems.db[15] = new Item(15, "MONEY", true, "MONEY")
DratewkaItems.db[16] = new Item(16, "a BARREL", true, "BARREL")
DratewkaItems.db[17] = new Item(17, "a sheeptrunk", false, "sheeptrunk")
DratewkaItems.db[18] = new Item(18, "BERRIES", true, "BERRIES")
DratewkaItems.db[19] = new Item(19, "WOOL", true, "WOOL")
DratewkaItems.db[20] = new Item(20, "a sheepskin", false, "sheepskin")
DratewkaItems.db[21] = new Item(21, "a BAG", true, "BAG")
DratewkaItems.db[22] = new Item(22, "a RAG", true, "RAG")
DratewkaItems.db[23] = new Item(23, "a sheephead", false, "sheephead")
DratewkaItems.db[24] = new Item(24, "a SPADE", true, "SPADE")
DratewkaItems.db[25] = new Item(25, "SULPHUR", true, "SULPHUR")
DratewkaItems.db[26] = new Item(26, "a solid poison", false, "solid poison")
DratewkaItems.db[27] = new Item(27, "a BUCKET", true, "BUCKET")
DratewkaItems.db[28] = new Item(28, "TAR", true, "TAR")
DratewkaItems.db[29] = new Item(29, "a liquid poison", false, "liquid poison")
DratewkaItems.db[30] = new Item(30, "a dead dragon", false, "dead dragon")
DratewkaItems.db[31] = new Item(31, "a STONE", true, "STONE")
DratewkaItems.db[32] = new Item(32, "a FISH", true, "FISH")
DratewkaItems.db[33] = new Item(33, "a KNIFE", true, "KNIFE")
DratewkaItems.db[34] = new Item(34, "a DRAGONSKIN", true, "DRAGONSKIN")
DratewkaItems.db[35] = new Item(35, "a pair of dragonskin SHOES", true, "SHOES")
DratewkaItems.db[36] = new Item(36, "a PRIZE", true, "PRIZE")
DratewkaItems.db[37] = new Item(37, "a SHEEP", true, "SHEEP")

for (let x = 1; x <= 7; x++) { DratewkaItems.map[x] = []; for (let y = 1; y <= 6; y++) { DratewkaItems.map[x][y] = []; } }
DratewkaItems.map[3][1] = [DratewkaItems.getById(31)]
DratewkaItems.map[5][1] = [DratewkaItems.getById(27)]
DratewkaItems.map[7][1] = [DratewkaItems.getById(14)]
DratewkaItems.map[3][2] = [DratewkaItems.getById(10)]
DratewkaItems.map[7][2] = [DratewkaItems.getById(18)]
DratewkaItems.map[2][3] = [DratewkaItems.getById(32)]
DratewkaItems.map[4][4] = [DratewkaItems.getById(21)]
DratewkaItems.map[5][5] = [DratewkaItems.getById(33)]
DratewkaItems.map[4][6] = [DratewkaItems.getById(24)]