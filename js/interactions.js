function Interaction(itemId, y, x, resultId, msg, isSheepPart, specialFunc) {
    this.itemId = itemId
    this.x = x
    this.y = y
    this.resultId = resultId
    this.msg = msg
    this.isSheepPart = isSheepPart
    this.specialFunc = specialFunc
}

var DratewkaInteractions = {
    db: [],

    forItemAtLocation: function (itemId, x, y) {
        return this.db.find((ia) => {
            return ia != undefined && ia.itemId == itemId
                && ia.x == x && ia.y == y
        })
    }
}

DratewkaInteractions.db.push(new Interaction(10, 5, 6, 11, "You opened a tool shed and took an axe", false))
DratewkaInteractions.db.push(new Interaction(11, 6, 7, 12, "You cut sticks for sheeplegs", false))
DratewkaInteractions.db.push(new Interaction(12, 4, 3, 13, "You prepared legs for your fake sheep", true))
DratewkaInteractions.db.push(new Interaction(14, 3, 4, 15, "The tavern owner paid you money", false))
DratewkaInteractions.db.push(new Interaction(15, 3, 7, 16, "The cooper sold you a new barrel", false))
DratewkaInteractions.db.push(new Interaction(16, 4, 3, 17, "You made a nice sheeptrunk", true))
DratewkaInteractions.db.push(new Interaction(18, 3, 6, 19, "The butcher gave you wool", false))
DratewkaInteractions.db.push(new Interaction(19, 4, 3, 20, "You prepared skin for your fake sheep", true))
DratewkaInteractions.db.push(new Interaction(21, 5, 7, 22, "You used your tools to make a rag", false))
DratewkaInteractions.db.push(new Interaction(22, 4, 3, 23, "You made a fake sheephead", true))
DratewkaInteractions.db.push(new Interaction(24, 1, 1, 25, "", false, function () {
    this.displayMessage("You are digging...", 2000, function () {
        this.displayMessage("and digging...", 2000, function () {
            this.inventory.splice(this.inventory.indexOf(DratewkaItems.getById(24)), 1)
            this.inventory.push(DratewkaItems.getById(25))
            this.displayMessage("That's enough sulphur for you", 2000)
        })
    })
}))
DratewkaInteractions.db.push(new Interaction(25, 4, 3, 26, "You prepared a solid poison", true))
DratewkaInteractions.db.push(new Interaction(27, 2, 1, 28, "You got a bucket full of tar", false))
DratewkaInteractions.db.push(new Interaction(28, 4, 3, 29, "You prepared a liquid poison", true))
DratewkaInteractions.db.push(new Interaction(37, 4, 3, 30, "", true, function () {
    this.displayMessage("The dragon noticed your gift...", 2000, function () {
        this.inventory.splice(this.inventory.indexOf(DratewkaItems.getById(37)), 1)
        DratewkaItems.addToLocationById(this.currentPosition.x, this.currentPosition.y, 30)

        DratewkaMap.At(3, 4).img = "DS68.bmp"
        this.dragonKilled = true

        this.displayMessage("The dragon ate your sheep and died!", 2000)
    })
}))
DratewkaInteractions.db.push(new Interaction(33, 4, 3, 34, "", false, function () {
    if (this.dragonKilled && DratewkaItems.isAtLocation(this.currentPosition.x, this.currentPosition.y, DratewkaItems.getById(30).name)) {
        this.inventory.splice(this.inventory.indexOf(DratewkaItems.getById(33)), 1)
        this.inventory.push(DratewkaItems.getById(34))

        this.displayMessage("You cut a piece of dragon's skin", 2000)
    } else
        this.displayMessage("Nothing happened", 2000)
}))
DratewkaInteractions.db.push(new Interaction(34, 5, 7, 35, "You used your tools to make shoes", false))
DratewkaInteractions.db.push(new Interaction(35, 4, 1, 36, "The King is impressed by your shoes", false))
DratewkaInteractions.db.push(new Interaction(36, 4, 1, 0, "", false, function () {
    this.inputBlocked = true
    //TODO: Ekran wygranej
    PageElements.victoryScreen.style.opacity = 1
}))