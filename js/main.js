var PageElements = {}

var DratewkaGame = {
    STARTING_POSITION: { x: 7, y: 4 },
    INVENTORY_SIZE: 1,
    ITEMS_PER_LOCATION: 3,
    currentPosition: null,
    currentLocation: null,
    inputBlocked: true,
    pressAnyKey: false,
    inventory: [],
    partsGathered: [],
    dragonKilled: false,
    capsLock: true,

    init: function () {
        this.currentPosition = this.STARTING_POSITION
        let moveCaret = function () {
            let offsetChars = PageElements.command.value.length <= 20 ? PageElements.command.value.length : 20
            PageElements.caret.style.left = (164 + offsetChars * 16) + "px"
        }
        PageElements.command.addEventListener("keydown", function (event) {
            if (this.pressAnyKey) { event.preventDefault(); this.update(); this.pressAnyKey = false; return; }
            if (this.inputBlocked) { event.preventDefault(); return; }
            if (event.code == "Tab" || event.code == "AltLeft" || event.code == "AltRight"
                || event.code == "ArrowUp" || event.code == "ArrowDown" || event.code == "ArrowLeft" || event.code == "ArrowRight") { event.preventDefault(); return; }
            else if (event.code == "Enter" && PageElements.command.value.length > 0) {
                let input = PageElements.command.value
                let cmd = input.split(" ")[0]
                let arg = ""
                if (input.includes(" ")) arg = input.substring(input.indexOf(" ") + 1, input.length)
                this.processCommand(cmd, arg)
                PageElements.command.value = ""
                return;
            } else if (event.code == "CapsLock") {
                event.preventDefault()
                this.capsLock = !this.capsLock
            } else if (event.key.length == 1 && PageElements.command.value.length < 20) {
                event.preventDefault()
                let charCase = event.shiftKey ? !this.capsLock : this.capsLock
                if (!charCase)
                    PageElements.command.value += event.key.toLowerCase()
                else
                    PageElements.command.value += event.key.toUpperCase()
            }
            setTimeout(moveCaret, 1)
        }.bind(this))

        let hejnal = new Audio("audio/hejnal.ogg")
        let introSkipped = false
        hejnal.addEventListener('loadeddata', () => {
            let skipIntro = function (e) {
                if (e.code == "Escape") {
                    window.removeEventListener("keydown", skipIntro)
                    introSkipped = true
                    hejnal.pause()
                    PageElements.introImage.style.display = "none"
                    PageElements.introA.style.display = "none"
                    PageElements.introB.style.display = "none"
                    DratewkaGame.inputBlocked = false
                    DratewkaGame.update()
                }
            }
            window.addEventListener("keydown", skipIntro)

            let introTime = hejnal.duration * 1000
            hejnal.play()
            setTimeout(() => {
                PageElements.introImage.style.opacity = 0
                setTimeout(() => {
                    PageElements.introA.style.opacity = 0
                    setTimeout(() => {
                        if (!introSkipped) {
                            window.removeEventListener("keydown", skipIntro)
                            PageElements.introB.style.opacity = 0
                            DratewkaGame.inputBlocked = false
                            DratewkaGame.update()
                        }
                    }, introTime * 0.35)
                }, introTime * 0.55)
            }, introTime * 0.1)
        })
    },

    update: function () {
        this.currentLocation = DratewkaMap.At(this.currentPosition.x, this.currentPosition.y)

        PageElements.locationText.innerText = this.currentLocation.text

        PageElements.locationImage.setAttribute("src", this.currentLocation.getImage())
        PageElements.locationImage.style.backgroundColor = this.currentLocation.color

        PageElements.compassDirections.North.style.display = this.currentLocation.hasPassage("North") ? "none" : "block";
        PageElements.compassDirections.South.style.display = this.currentLocation.hasPassage("South") ? "none" : "block";
        PageElements.compassDirections.East.style.display = this.currentLocation.hasPassage("East") ? "none" : "block";
        PageElements.compassDirections.West.style.display = this.currentLocation.hasPassage("West") ? "none" : "block";

        PageElements.info.style.fontSize = "initial"
        PageElements.info.style.lineHeight = "125%"

        let directions = []
        if (this.currentLocation.hasPassage("West")) directions.push("WEST")
        if (this.currentLocation.hasPassage("North")) directions.push("NORTH")
        if (this.currentLocation.hasPassage("East")) directions.push("EAST")
        if (this.currentLocation.hasPassage("South")) directions.push("SOUTH")
        PageElements.info.innerText = "You can go"
        PageElements.info.innerText += " " + directions[0]
        if (directions.length > 2)
            for (let i = 1; i < directions.length - 1; i++) PageElements.info.innerText += ", " + directions[i]
        if (directions.length > 1)
            PageElements.info.innerText += " and " + directions[directions.length - 1]
        PageElements.info.innerText += "\n"

        let items = DratewkaItems.atLocation(this.currentPosition.x, this.currentPosition.y)
        let itemLabels = []
        for (let item of items) { itemLabels.push(item.label) }
        PageElements.info.innerText += "You see"
        PageElements.info.innerText += itemLabels.length > 0 ? " " + itemLabels[0] : " nothing"
        if (itemLabels.length > 2)
            for (let i = 1; i < itemLabels.length - 1; i++) PageElements.info.innerText += ", " + itemLabels[i]
        if (itemLabels.length > 1)
            PageElements.info.innerText += " and " + itemLabels[itemLabels.length - 1]
        PageElements.info.innerText += "\n"

        itemLabels = []
        for (let item of this.inventory) { itemLabels.push(item.label) }
        PageElements.info.innerText += "You are carrying"
        PageElements.info.innerText += itemLabels.length > 0 ? " " + itemLabels[0] : " nothing"
        if (itemLabels.length > 2)
            for (let i = 1; i < itemLabels.length - 1; i++) PageElements.info.innerText += ", " + itemLabels[i]
        if (itemLabels.length > 1)
            PageElements.info.innerText += " and " + itemLabels[itemLabels.length - 1]
        PageElements.info.innerText += "\n"

        PageElements.inputField.style.opacity = 1
        PageElements.command.focus()
        this.inputBlocked = false;
    },

    displayMessage: function (msg, duration) {
        PageElements.inputField.style.opacity = 0
        PageElements.info.innerText = msg
        this.inputBlocked = true
        if (arguments[2])
            setTimeout(arguments[2].bind(this), duration)
        else
            setTimeout(this.update.bind(this), duration)
    },

    processCommand: function (cmd, arg) {
        arg = arg || ""
        switch (cmd.toUpperCase()) {
            case 'E': case 'EAST':
                if (this.currentLocation.hasPassage("East")) {
                    this.currentPosition.x += 1
                    this.displayMessage("You are going East...", 1000)
                } else {
                    this.displayMessage("You can't go East from here.", 2000)
                }
                break
            case 'W': case 'WEST':
                if (this.currentLocation.hasPassage("West")) {
                    if (this.currentPosition.x == 2 && this.currentPosition.y == 4 && !this.dragonKilled) {
                        this.displayMessage("You can't go that way...", 2000, function () {
                            this.displayMessage("The dragon sleeps in a cave!", 2000)
                        })
                    } else {
                        this.currentPosition.x -= 1
                        this.displayMessage("You are going West...", 1000)
                    }
                } else {
                    this.displayMessage("You can't go West from here.", 2000)
                }
                break
            case 'N': case 'NORTH':
                if (this.currentLocation.hasPassage("North")) {
                    this.currentPosition.y -= 1
                    this.displayMessage("You are going North...", 1000)
                } else {
                    this.displayMessage("You can't go North from here.", 2000)
                }
                break
            case 'S': case 'SOUTH':
                if (this.currentLocation.hasPassage("South")) {
                    this.currentPosition.y += 1
                    this.displayMessage("You are going South...", 1000)
                } else {
                    this.displayMessage("You can't go South from here.", 2000)
                }
                break
            case 'T': case 'TAKE':
                if (this.inventory.length == this.INVENTORY_SIZE) {
                    this.displayMessage("You are already carrying something", 2000)
                    break
                }
                if (DratewkaItems.tryRemoveFromLocation(this.currentPosition.x, this.currentPosition.y, arg)) {
                    this.inventory.push(DratewkaItems.getByName(arg))
                    this.displayMessage("You are taking " + DratewkaItems.getByName(arg).label, 2000)
                } else {
                    if (DratewkaItems.isAtLocation(this.currentPosition.x, this.currentPosition.y, arg))
                        this.displayMessage("You can't carry it", 2000)
                    else
                        this.displayMessage("There isn't anything like that here", 2000)
                }
                break
            case 'D': case 'DROP':
                if (this.inventory.length == 0) {
                    this.displayMessage("You are not carrying anything", 2000)
                    break
                }
                if (DratewkaItems.atLocation(this.currentPosition.x, this.currentPosition.y).length == this.ITEMS_PER_LOCATION) {
                    this.displayMessage("You can't store any more here", 2000)
                    break
                }
                let itemFound = false
                for (let item of this.inventory) {
                    if (item.name.toUpperCase() == arg.toUpperCase()) {
                        this.inventory.splice(this.inventory.indexOf(item), 1)
                        itemFound = true
                    }
                }
                if (itemFound) {
                    DratewkaItems.addToLocationByName(this.currentPosition.x, this.currentPosition.y, arg)
                    this.displayMessage("You are about to drop " + DratewkaItems.getByName(arg).label, 2000)
                } else {
                    this.displayMessage("You are not carrying it", 2000)
                }
                break
            case 'U': case 'USE':
                let usedItem = null
                for (let item of this.inventory) {
                    if (item.name.toUpperCase() == arg.toUpperCase()) {
                        usedItem = item
                    }
                }
                if (usedItem) {
                    let interaction = DratewkaInteractions.forItemAtLocation(usedItem.id, this.currentPosition.x, this.currentPosition.y)
                    if (interaction) {
                        if (interaction.specialFunc)
                            interaction.specialFunc.bind(this)()
                        else {
                            this.inventory.splice(this.inventory.indexOf(usedItem), 1)

                            if (interaction.isSheepPart) {
                                if (this.addSheepPart(DratewkaItems.getById(interaction.resultId).name)) {
                                    this.displayMessage("Your fake sheep is full of poison and ready to be eaten by the dragon", 2000)
                                } else {
                                    DratewkaItems.addToLocationById(this.currentPosition.x, this.currentPosition.y, interaction.resultId)
                                    this.displayMessage(interaction.msg, 2000)
                                }
                            } else {
                                this.inventory.push(DratewkaItems.getById(interaction.resultId))
                                this.displayMessage(interaction.msg, 2000)
                            }
                        }
                    } else
                        this.displayMessage("Nothing happened", 2000)
                } else
                    this.displayMessage("You aren't carrying anything like that", 2000)
                break
            case 'G': case 'GOSSIPS':
                PageElements.info.style.fontSize = "small"
                PageElements.info.style.lineHeight = "110%"
                PageElements.info.innerText = "The woodcutter lost his home key..."
                PageElements.info.innerText += "\nThe butcher likes fruit..."
                PageElements.info.innerText += "\nThe cooper is greedy..."
                PageElements.info.innerText += "\nDratewka plans to make a poisoned bait for the dragon..."
                PageElements.info.innerText += "\nThe tavern owner is buying food from the pickers..."
                PageElements.info.innerText += "\nMaking a rag from a bag..."
                PageElements.info.innerText += "\nPress any key"
                this.pressAnyKey = true
                PageElements.inputField.style.opacity = 0
                break
            case 'V': case 'VOCABULARY':
                PageElements.info.innerText = "NORTH or N, SOUTH or S"
                PageElements.info.innerText += "\nWEST or W, EAST or E"
                PageElements.info.innerText += "\nTAKE (object) or T (object)"
                PageElements.info.innerText += "\nDROP (object) or D (object)"
                PageElements.info.innerText += "\nUSE (object) or U (object)"
                PageElements.info.innerText += "\nGOSSIPS or G, VOCABULARY or V"
                PageElements.info.innerText += "\nPress any key"
                this.pressAnyKey = true
                PageElements.inputField.style.opacity = 0
                break
            default:
                PageElements.info.innerText = "Try another word or V for vocabulary"
                PageElements.inputField.style.opacity = 0
                this.inputBlocked = true
                setTimeout(this.update.bind(this), 2000)
                break
        }
    },

    addSheepPart(partName) {
        this.partsGathered[partName] = true
        if (this.partsGathered["sheeplegs"] && this.partsGathered["sheeptrunk"] && this.partsGathered["sheepskin"]
            && this.partsGathered["sheephead"] && this.partsGathered["solid poison"] && this.partsGathered["liquid poison"]) {
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "sheeplegs")
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "sheeptrunk")
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "sheepskin")
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "sheephead")
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "solid poison")
            DratewkaItems.removeSheepPart(this.currentPosition.x, this.currentPosition.y, "liquid poison")

            this.inventory.push(DratewkaItems.getByName("SHEEP"))

            return true
        }
        return false
    }
}

document.addEventListener("DOMContentLoaded", function (event) {
    PageElements = {
        game: document.getElementById("game"),
        locationText: document.getElementById("locationText"),
        locationImage: document.getElementById("locationImage"),
        compass: document.getElementById("compass"),
        compassDirections: {
            North: document.getElementById("compassNorth"),
            South: document.getElementById("compassSouth"),
            East: document.getElementById("compassEast"),
            West: document.getElementById("compassWest"),
        },
        info: document.getElementById("info"),
        command: document.getElementById("command"),
        inputField: document.getElementById("inputField"),
        introImage: document.getElementById("introImage"),
        introA: document.getElementById("introA"),
        introB: document.getElementById("introB"),
        victoryScreen: document.getElementById("victoryScreen"),
        caret: document.getElementById("caret")
    }

    DratewkaGame.init()
})